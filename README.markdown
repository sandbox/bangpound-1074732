!&# Content
===========

This Drupal 6 module uses ctools content to render different types of content
in different formats.

This was developed for the sole purpose supporting arbitrary content in CiviCRM
Dashlets. Any other use is untested.

Usage
-----

The module sets up a single page callback `bp-content`. At least two additional
arguments are required, and possibly more.

    bp-content/FORMAT/TYPE/SUBTYPE

* **FORMAT**  
  I use the `ctools_js` load argument incorrectly, because I wanted to support
  formats besides ctools JSON objects. Valid values are `ajax`, `nojs`, `ahah`,
  `json`, `jsonp`.
* **TYPE**  
  The name of any Ctools content type plugin, such as `block`, `contact`,
  `node`, `page_title`. Context is not supported.
* **SUBTYPE**  
  The subtype, which may not be required depending on your **TYPE**. For
  `block`, you can specify the module and delta as the subtype: `user-1` for the
  Navigation menu.

Depending on the **FORMAT** value, other GET variables must be included.

* `jsonp` requires `callback`.
* `ajax` requires `selector`.

You must also pass the ctools content type. Depending on the ctools content
type, subtype and other arguments may also be required.

Examples
--------

* Navigation menu (User module's block #1) as AHAH fragment.  
  `bp-content/ahah/block/user-1`
* Contact form page callback.  
  `bp-content/nojs/contact`
* Contact form JSON object.  
  `bp-content/json/contact`
* Site name as a page callback.  
  `bp-content/nojs/page_site_name`
* AHAH fragment of node 1's teaser.  
  `bp-content/ahah/node&conf={%22nid%22:%221%22,%22leave_node_title%22:true}`

CiviCRM Dashlets
================

To create a CiviCRM dashlet, just add a row to the civicrm_dashboard table.

    INSERT INTO `civicrm_dashboard` (`domain_id`,`label`,`url`,`permission`,`is_active`)
    VALUES
      (1, 'Navigation menu', 'bp-content/ahah/block/user-1', 'access CiviCRM', 1);

I had to remove a dashlet from my dashboard before I the new one appeared in the
box of available dashlets.
