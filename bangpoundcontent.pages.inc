<?php
// $Id$
/**
 * @file bangpoundcontent.pages.inc
 * TODO: Enter file description here.
 */

/**
 * Handles rendering of ctools content using URL arguments and GET variables.
 */
function bangpoundcontent_content_render($js = NULL, $type, $subtype = NULL) {
  ctools_include('content');

  // `conf` value must be an array, so it is JSON encoded because I couldn't
  // think of a better way and I'm not even using it yet.

  $conf = filter_has_var(INPUT_GET, 'conf') ?
    (array) json_decode(filter_input(INPUT_GET, 'conf', FILTER_SANITIZE_URL)) :
    array();

  $block = ctools_content_render($type, $subtype, $conf);
  drupal_set_title($block->title);

  // ctools ajax
  if ($js) {
    ctools_include('ajax');
    $commands = array();

    // Pass an encoded value to GET variable `selector`
    $selector = filter_input(INPUT_GET, 'selector', FILTER_SANITIZE_ENCODED);
    $commands[] = ctools_ajax_command_html($selector, $block->content);
    ctools_ajax_render($commands);
  }
  else {

    // We want to handle more than just ctools ajax rendering. Arg 1 is
    //  - ajax - rendered above.
    //  - nojs - drupal themed page
    //  - ahah - echo content and exit.
    //  - json - core drupal_json() call.
    //  - jsonp - use callback GET variable.
    switch (arg(1)) {

    // HTML fragment.
    case 'ahah':
      echo $block->content;
      break;

    // Render just the JSON representation of the content object.
    case 'json':
      drupal_json($block);
      break;

    // .. with a JSONP callback.
    case 'jsonp':
      drupal_set_header('Content-Type: text/javascript; charset=utf-8');
      $callback = filter_input(INPUT_GET, 'callback', FILTER_SANITIZE_ENCODED);
      echo $callback .'('. drupal_to_js($block) .');';
      break;

    // Behave as a normal Drupal page callback, render through page theme.
    case 'nojs':
    default:
      return $block->content;
    }
  }
}
